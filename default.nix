{ pkgs ? import <nixpkgs> {} }:

# copy existing sdk into
# error: SDK root '/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk' does not exist
# sudo ln -s /Library/Developer/CommandLineTools/SDKs/MacOSX10.15.sdk \
#            /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk
# don't worry abount compatibility - content doesn't have any sense
# only name is matter.
with pkgs;
with pkgs.lib;
with pkgs.python39Packages;

let
  pyobjc-core = buildPythonPackage rec {
    pname = "pyobjc-core";
    version = "7.3";
    name = "${pname}-${version}";
    src = pkgs.python39Packages.fetchPypi {
      pname = "pyobjc-core";
      inherit version;
      sha256 = "0x3msrzvcszlmladdpl64s48l52fwk4xlnnri8daq2mliggsx0ah";
    };
    # setupPyBuildFlags = [ "-j" "12" ] ;
    #makeWrapperArgs = ["--set SDKROOT /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"];
    preBuild=''
         export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"
         # export LDFLAGS="$LDFLAGS -L/usr/local/opt/libffi/lib"
         # export LIBRARY_PATH=/usr/local/opt/libffi/lib
         # export DYLD_LIBRARY_PATH=/usr/local/opt/libffi/lib
    '';
    # DYLD_LIBRARY_PATH=/usr/local/opt/libffi/lib;
    # LIBRARY_PATH=/usr/local/opt/libffi/lib;
    CFLAGS = "-iwithsysroot /usr/include -Wno-unused-argument";
    # LDFLAGS =  "-L/usr/local/opt/libffi/lib";
    # /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk/usr/lib";
    # -L/usr/local/lib -L/usr/local/opt/libffi/lib";
    # setupPyBuildFlags = [ "-j 10" ] ;
    # pipInstallFlags=["--install-option='-j 11'"];
    # CFLAGS = "-Wno-unused-argument";
    # CFLAGS="-iwithprefix /usr/include -iwithsysroot /usr/include -Wno-unused-argument -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    # SDKROOT = "/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    doCheck = false;
    commonPreHook = ''
      export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    '';
    propagatedBuildInputs = [
      pkgs.darwin.libobjc
      pkgs.darwin.cctools
      pkgs.darwin.apple_sdk.frameworks.Foundation
      pkgs.darwin.apple_sdk.frameworks.AppKit
      pkgs.darwin.apple_sdk.frameworks.CoreServices
      pkgs.darwin.apple_sdk.frameworks.Cocoa
      pkgs.python39Packages.setuptools
    ];
    buildInputs = [ pkgs.libffi ];
  };

  cocoa = pkgs.python39Packages.buildPythonPackage rec {
    pname = "pyobjc";
    version = "7.3";
    name = "${pname}-${version}";
    src = pkgs.python39Packages.fetchPypi {
      pname = "pyobjc-framework-Cocoa";
      inherit version;
      sha256 = "0zhbp18i06aprwfbp06l9wm3qrzsdcyy9hwis5d4b8wmlzkhb3di"; # 7.3 Cocoa
    };
    CFLAGS="-Wno-unused-argument -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    doCheck = false;
    setupPyBuildFlags = [ "-j" "12" ] ;
    propagatedBuildInputs = [
      pyobjc-core
      pkgs.darwin.libobjc
      pkgs.darwin.apple_sdk.frameworks.Foundation
      pkgs.darwin.apple_sdk.frameworks.CoreFoundation
      # pkgs.darwin.apple_sdk.frameworks.AVKit
      pkgs.darwin.apple_sdk.frameworks.AppKit
      pkgs.darwin.apple_sdk.frameworks.Cocoa
      # pkgs.darwin.apple_sdk.frameworks.AVFoundation
      # pkgs.darwin.apple_sdk.frameworks.DVDPlayback
      pkgs.darwin.apple_sdk.frameworks.CoreServices
      # pkgs.darwin.apple_sdk.frameworks.Accounts
      # pkgs.darwin.apple_sdk.frameworks.IMServicePlugIn
      pkgs.python39Packages.setuptools
    ];
  };

  my-send2trash = pkgs.python39Packages.buildPythonPackage rec {
    name = "send2trash";
    version = "1.7.1";

    doCheck = !pkgs.stdenv.isDarwin;
    setupPyBuildFlags = [ "-j" "12" ] ;
    src = pkgs.fetchFromGitHub {
      owner = "arsenetar";
	    repo = "${name}";
	    rev = "${version}";
	    sha256 = "04fyax0jbqvichv1w5gbi5jy3zj3p2npdqq8k2prripl7xam6na9";
    };

    # propagatedBuildInputs = [
      # pkgs.darwin.libobjc
      # pkgs.darwin.apple_sdk.frameworks.Foundation
      # pkgs.darwin.apple_sdk.frameworks.CoreFoundation
    # ] ++
    propagatedBuildInputs = (with pkgs.python39Packages; [ pyobjc-core cocoa setuptools pytest ]);
  };

  foo = pkgs.stdenv.mkDerivation rec {
    pname = "foo";
    version = "0.1";
    src = ./.;
    buildInputs = [ my-send2trash ];
    # pkgs.clang pkgs.darwin.libobjc pkgs.darwin.apple_sdk.frameworks.Foundation ];
  };

  my-appnope = pkgs.python39Packages.buildPythonPackage rec {
    pname = "appnope";
    version = "0.1.2";

    src = pkgs.python39Packages.fetchPypi {
      inherit pname version;
      sha256 = "dd83cd4b5b460958838f6eb3000c660b1f9caf2a5b1de4264e941512f603258a";
    };

    meta = {
      description = "Disable App Nap on macOS";
      homepage    = "https://pypi.python.org/pypi/appnope";
      platforms   = platforms.darwin;
      license     = licenses.bsd3;
    };
    propagatedBuildInputs = [
      pyobjc-core
      cocoa
      setuptools
      pytest
      pkgs.darwin.apple_sdk.frameworks.Foundation
      pkgs.darwin.apple_sdk.frameworks.CoreFoundation
      pkgs.darwin.apple_sdk.frameworks.AppKit
      pkgs.darwin.apple_sdk.frameworks.Cocoa
      pkgs.darwin.apple_sdk.frameworks.CoreServices
    ];
    doCheck = false;
  };

  # cpythonBuildInputs = ((filter (p: p != null) (
  #     [zlib bzip2 expat xz libffi gdbm sqlite readline ncurses openssl tzdata ]))
  # ++ [ darwin.configd ]);

  # sourceVersion = {major = "3"; minor = "9"; };
  # pythonAttr = "python${sourceVersion.major}${sourceVersion.minor}";




  # passthru = rec { # passthruFun rec {
  #   inherit self sourceVersion packageOverrides;
  #   implementation = "cpython";
  #   pkgs = pkgs.python39Packages;
  #   libPrefix = "python${pythonVersion}";
  #   executable = libPrefix;
  #   pythonVersion = with sourceVersion; "${major}.${minor}";
  #   sitePackages = "lib/${libPrefix}/site-packages";
  #   # inherit hasDistutilsCxxPatch;
  #   pythonOnBuildForBuild = pkgsBuildBuild.${pythonAttr};
  #   pythonOnBuildForHost = pkgsBuildHost.${pythonAttr};
  #   pythonOnBuildForTarget = pkgsBuildTarget.${pythonAttr};
  #   pythonOnHostForHost = pkgsHostHost.${pythonAttr};
  #   pythonOnTargetForTarget = pkgsTargetTarget.${pythonAttr} or {};
  # };

  # my-cpython = pkgs.stdenv.mkDerivation {
  #   pname = "python3";
  #   version = "3.11.5-master"; # branch of master
  #   doCheck = !pkgs.stdenv.isDarwin;
  #   # setupPyBuildFlags = [ "-j" "12" ] ;
  #   inherit passthru;
  #   enableParallelBuilding = true;
  #   CFLAGS="-Wno-unused-command-line-argument -Wno-strict-prototypes -Wno-implicit-function-declaration";

  #   # Determinism: We fix the hashes of str, bytes and datetime objects.
  #   PYTHONHASHSEED=0;

  #   prePatch = optionalString stdenv.isDarwin ''
  #   substituteInPlace configure --replace '`/usr/bin/arch`' '"i386"'
  #   substituteInPlace configure --replace '-Wl,-stack_size,1000000' ' '
  # '' ;

  #   patches = [ ./3.7/darwin-libutil.patch ];
  #   nativeBuildInputs = [ nukeReferences ];

  #   preConfigure = ''
  #   for i in /usr /sw /opt /pkg; do	# improve purity
  #     substituteInPlace ./setup.py --replace $i /no-such-path
  #   done
  # '' + pkgs.lib.optionalString pkgs.stdenv.isDarwin ''
  #   export NIX_CFLAGS_COMPILE="$NIX_CFLAGS_COMPILE -msse2"
  #   export MACOSX_DEPLOYMENT_TARGET=10.6
  #   # Determinism: The interpreter is patched to write null timestamps when compiling Python files
  #   #   so Python doesn't try to update the bytecode when seeing frozen timestamps in Nix's store.
  #   export DETERMINISTIC_BUILD=1;
  # '' + pkgs.lib.optionalString pkgs.stdenv.hostPlatform.isMusl ''
  #   export NIX_CFLAGS_COMPILE+=" -DTHREAD_STACK_SIZE=0x100000"

  # '';

  #   src = pkgs.fetchFromGitHub {
  #     owner = "bergkvist";
	#     repo = "cpython";
	#     rev = "01bcf2bef5f4ffffb454da35cb66b186a7a12598";
	#     sha256 = "1713sx5izd745bgr6fdx6d1g7ivaqy6jrf9v5jgml31bd1nmfccy";
  #   };

  #   configureFlags = [
  #     "--enable-shared"
  #     "--without-ensurepip"
  #     "--with-system-expat"
  #     "--with-system-ffi"
  #   ];

  #   propagatedBuildInputs = [
  #     pkgs.darwin.libobjc
  #     pkgs.darwin.cctools
  #     pkgs.darwin.apple_sdk.frameworks.Foundation
  #   ]; # pyobjc-core cocoa setuptools pytest ]);

  #   buildInputs = cpythonBuildInputs;


  #   LDFLAGS = concatStringsSep " " (map (p: "-L${getLib p}/lib") cpythonBuildInputs);
  #   CPPFLAGS ="-Wno-error " + concatStringsSep " " (map (p: "-I${getDev p}/include") cpythonBuildInputs);
  # };


  myPatchedPython = (pkgs.python3.overrideAttrs (old:  {
    src = pkgs.fetchFromGitHub {
      owner = "bergkvist";
	    repo = "cpython";
	    rev = "01bcf2bef5f4ffffb454da35cb66b186a7a12598";
	    sha256 = "1713sx5izd745bgr6fdx6d1g7ivaqy6jrf9v5jgml31bd1nmfccy";
    };
    verions = "3.11.5";

    # prePatch = ''
    # cp -r 3.10 3.11
    # '' + old.prePatch;
    patches = (builtins.tail old.patches) ++ [
      (./. + "/3.11/no-ldconfig.patch")
    ];

    postInstall = ''
    '' + old.postInstall;

    preBuild=''
         export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"
    '';

    commonPreHook = ''
      export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    '';

    buildInputs = old.buildInputs ++ [
      pkgs.tcl-8_6
      pkgs.tk-8_6
      pkgs.darwin.apple_sdk.frameworks.Tcl
      # pkgs.darwin.apple_sdk.frameworks.Tk
    ];
    doCheck = false;
    LDFLAGS = " -L${pkgs.tcl-8_6}/lib -L${pkgs.tk-8_6}/lib ";
  })).override {
    # not working :(
    libX11 = null;
    stripTkinter = true;
    tcl = null;
    tk = null;
    tix = null;
    xorgproto = null;
    x11Support = false;
    sourceVersion = {major = "3"; minor = "11" ; patch = "5"; suffix = ""; };
  };

  myPkgs = pkgs.extend (self: super: {
    python3 = myPatchedPython;
    liblqr1 = super.liblqr1.overrideAttrs(old: {
      propagatedBuildInputs = [
        pkgs.darwin.apple_sdk.frameworks.Carbon
      ] ++ old.propagatedBuildInputs; # buildInputs;

      CFLAGS="-Wno-unused-argument -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
      doCheck = false;
      preBuild=''
         export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"
        '';

      commonPreHook = ''
          export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
       '';
    });
  });

  pkgsFixed = myPkgs.python39Packages.override (
    old: {
      overrides = builtins.foldl' pkgs.lib.composeExtensions (old.overrides or (_: _: {}))
        [ overrideSend2Trash disableCheck ];
    }
  );

  overrideSend2Trash = self: super:
    {
      send2trash = my-send2trash;
      #python3Packages.send2trash = my-send2trash;
      # pythonPackages.send2trash = my-send2trash;
      appnope = my-appnope;
      pyzmq = super.pyzmq.overridePythonAttrs(old: {
        doCheck = false;
      });
    };
  disableCheck = self: super:
    {
      jupyter_server = super.jupyter_server.overridePythonAttrs(old: {
        doCheck = false;
      });
      notebook = super.notebook.overridePythonAttrs(old: {
        doCheck = false;
      });
      ipykernel = super.ipykernel.overridePythonAttrs(old: {
        doCheck = false;
      });
    };
  # pkgsFixed = import <nixpkgs> { overlays = [ overrideSend2Trash ]; } ;


in pkgs.mkShell {
  buildInputs = [
    /*python39Packages.send2trash*/
    # (pkgsFixed.jupyter_server)
    (pkgsFixed.notebook)
    # myPkgs.liblqr1
    # my-cpython
    # my-appnope
  ] ;
} #  foo

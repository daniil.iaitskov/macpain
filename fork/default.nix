{ pkgs ? import /Users/ec2-user/pro/nixpkgs {} }:
#{ pkgs ? import (builtins.fetchTarball { url = "https://github.com/NixOS/nixpkgs/archive/refs/heads/nixpkgs-21.05-darwin.tar.gz"; }) {} }:

let
  pytest-ovr = hf: hs: {
    # pytest = hs.pytest.overridePythonAttrs(old: {
    #   doCheck = false;
    # });
    toml = hs.toml.overridePythonAttrs(old: {
      doCheck = false;
    });
    six = hs.six.overridePythonAttrs(old: {
      doCheck = false;
    });
    semver = hs.pytest.overridePythonAttrs(old: {
      doCheck = false;
    });
    brotli = hs.brotli.overridePythonAttrs(old: {
      doCheck = false;
    });
    # hypothesis = hs.hypothesis.overrideAttrs (old: {
    #   doCheck = false;
    # });
    # pytest-xdist = hs.pytest-xdist.overrideAttrs (old: {
    #   doCheck = false;
    # });
    pyzmq = hs.pyzmq.overridePythonAttrs (old: {
      doCheck = false;
    });
  };

  # py-pkgs = (pkgs.python39Packages);

  # py-pkgs = pkgs.python39Packages.override (
  #   old: {
  #     overrides = builtins.foldl' pkgs.lib.composeExtensions (old.overrides or (_: _: {}))
  #       [ pytest-ovr ];
  #   }
  # );

  myPkgs = pkgs.extend (self: super: rec {
    python39 = super.python39.override {
      packageOverrides = self: super: {
        pytest = super.pytest.overrideAttrs (old: {
          doCheck = false;
        });
        # hypothesis = super.hypothesis.overridePythonAttrs (old: {
        #   doCheck = false;
        # });
      };
    };
    python39Packages = (python39.pkgs.override (
      old: {
        overrides = builtins.foldl' pkgs.lib.composeExtensions (old.overrides or (_: _: {}))
          [ pytest-ovr ];
      }
    ));
  });

  py-pkgs = myPkgs.python39Packages;

  pyobjc-core = py-pkgs.buildPythonPackage rec {
    pname = "pyobjc-core";
    version = "7.3";
    adfasdf = true;
    name = "${pname}-${version}";
    src = py-pkgs.fetchPypi {
      pname = "pyobjc-core";
      inherit version;
      sha256 = "0x3msrzvcszlmladdpl64s48l52fwk4xlnnri8daq2mliggsx0ah";
    };
    # setupPyBuildFlags = [ "-j" "12" ] ;
    #makeWrapperArgs = ["--set SDKROOT /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"];
    preBuild=''
         export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"
    '';
    CFLAGS = "-iwithsysroot /usr/include -Wno-unused-argument";
    doCheck = false;
    commonPreHook = ''
      export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    '';
    propagatedBuildInputs = [
      pkgs.darwin.libobjc
      pkgs.darwin.cctools
      pkgs.darwin.apple_sdk.frameworks.Foundation
      pkgs.darwin.apple_sdk.frameworks.AppKit
      pkgs.darwin.apple_sdk.frameworks.CoreServices
      pkgs.darwin.apple_sdk.frameworks.Cocoa
      py-pkgs.setuptools
    ];
    buildInputs = [ pkgs.libffi ];
  };

  cocoa = py-pkgs.buildPythonPackage rec {
    pname = "pyobjc";
    version = "7.3";
    name = "${pname}-${version}";
    src = py-pkgs.fetchPypi {
      pname = "pyobjc-framework-Cocoa";
      inherit version;
      sha256 = "0zhbp18i06aprwfbp06l9wm3qrzsdcyy9hwis5d4b8wmlzkhb3di"; # 7.3 Cocoa
    };
    CFLAGS="-Wno-unused-argument -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    doCheck = false;
    setupPyBuildFlags = [ "-j" "12" ] ;
    propagatedBuildInputs = [
      pyobjc-core
      pkgs.darwin.libobjc
      pkgs.darwin.apple_sdk.frameworks.Foundation
      pkgs.darwin.apple_sdk.frameworks.CoreFoundation
      pkgs.darwin.apple_sdk.frameworks.AppKit
      pkgs.darwin.apple_sdk.frameworks.Cocoa
      pkgs.darwin.apple_sdk.frameworks.CoreServices
      py-pkgs.setuptools
    ];
  };

  my-send2trash = py-pkgs.buildPythonPackage rec {
    name = "send2trash";
    version = "1.7.1";

    doCheck = !pkgs.stdenv.isDarwin;
    setupPyBuildFlags = [ "-j" "12" ] ;
    src = pkgs.fetchFromGitHub {
      owner = "arsenetar";
	    repo = "${name}";
	    rev = "${version}";
	    sha256 = "04fyax0jbqvichv1w5gbi5jy3zj3p2npdqq8k2prripl7xam6na9";
    };

    propagatedBuildInputs = (with py-pkgs; [ pyobjc-core cocoa setuptools pytest ]);
  };

  my-appnope = myPkgs.python39Packages.buildPythonPackage rec {
    pname = "appnope";
    version = "0.1.2";

    # src = py-pkgs.fetchPypi {
    #   inherit pname version;
    #   sha256 = "dd83cd4b5b460958838f6eb3000c660b1f9caf2a5b1de4264e941512f603258a";
    # };

    src = pkgs.fetchFromGitHub {
      owner = "yaitskov";
	    repo = "appnope";
	    rev = "24cd8b6d3d6586eb365a25ab235f12439cee1f87";
	    sha256 = "1va8xj4nrvn2jcyr5r7r2klz0a1nq5qxvyx29mh2lwkm1a9s2qf9";
    };

    meta = {
      description = "Disable App Nap on macOS";
      homepage    = "https://pypi.python.org/pypi/appnope";
      platforms   = pkgs.lib.platforms.darwin;
      license     = pkgs.lib.licenses.bsd3;
    };
    propagatedBuildInputs = (with py-pkgs; [
      pyobjc-core
      cocoa
      setuptools
      pytest
      pkgs.darwin.apple_sdk.frameworks.Foundation
      pkgs.darwin.apple_sdk.frameworks.CoreFoundation
      pkgs.darwin.apple_sdk.frameworks.AppKit
      pkgs.darwin.apple_sdk.frameworks.Cocoa
      pkgs.darwin.apple_sdk.frameworks.CoreServices
    ]);
    doCheck = false;
  };

  overrideSend2Trash = self: super:
    {
      send2trash = my-send2trash;
      appnope = my-appnope;
    };

  disableCheck = self: super:
    {
      # send2trash = my-send2trash;
      # appnope = my-appnope;
      jupyter_server = super.jupyter_server.overridePythonAttrs(old: {
        doCheck = false;
      });
      notebook = (super.notebook.overridePythonAttrs(old: {
        doCheck = false;
      }));
      ipykernel = super.ipykernel.overridePythonAttrs(old: {
        doCheck = false;
      });
    };

  pkgsFixed = py-pkgs.override (
    old: {
      overrides = builtins.foldl' pkgs.lib.composeExtensions (old.overrides or (_: _: {}))
        [ overrideSend2Trash disableCheck ];
    }
  );
in
 pkgs.mkShell {
  doCheck = false;
  buildInputs = [ /*pkgsFixed.appnope*/ pkgsFixed.notebook ] ;
}


# (pkgsFixed.python39.withPackages (p: with p; [ appnope ])).env

#  pkgs.mkShell {
#   doCheck = false;
#   buildInputs = [ pkgsFixed.appnope /*pkgsFixed.notebook*/ ] ;
# }

#in (myPkgs.python39.withPackages (p: with p; [ pygit2 my-send2trash ])).env

# in (x.withPackages (p: with p; [ pygogo  ])).env

/*

*/

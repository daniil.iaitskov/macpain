{ pkgs ? import (builtins.fetchTarball { url = "https://github.com/NixOS/nixpkgs/archive/refs/heads/nixpkgs-21.05-darwin.tar.gz"; }) {} }:

let
  myPatchedPython = (pkgs.python39.overrideAttrs (old:  {
    # src = pkgs.fetchFromGitHub {
    #   owner = "python";
	  #   repo = "cpython";
    #   rev = "7dad0337518a0d599caf8f803a5bf45db67cbe9b";
	  #   sha256 = "1mwbbg4jwjwfc6m8vfzc8ifzpq9hqavk7687hdjjqh5bggb0pvk1";

    #   #rev = "0a7dcbdb13f1f2ab6e76e1cff47e80fb263f5da0"; # 3.9.5
    #   #sha256 = "0fw1bdphhahr4syahd6ii732ryfpxzlbpxjc3r81bi05yvhybd3q";
    # };

    preBuild=''
      export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"
    '';

    # commonPreHook = ''
    #   # export LIBFFI_INCLUDEDIR=${pkgs.libffi.dev}/include
    #   export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    # '';

    # LIBFFI_INCLUDEDIR="${pkgs.libffi.dev}/include";
    # CFLAGS="-iwithprefix ${pkgs.libffi.dev}/include";

    # buildInputs = old.buildInputs ++ [
    #   # pkgs.libffi
    #   # pkgs.tcl-8_6
    #   # pkgs.tk-8_6
    #   pkgs.darwin.apple_sdk.frameworks.Tcl
    # ];
    doCheck = false;
    # LDFLAGS = " -lm";
    # LDFLAGS = " -L${pkgs.tcl-8_6}/lib -L${pkgs.tk-8_6}/lib " + old.LDFLAGS;
  })).override { self = myPatchedPython; };

  # py-pkgs = pkgs.python39Packages; #
  py-pkgs = myPatchedPython.pkgs ; #.python39Packages; #

  pyobjc-core = py-pkgs.buildPythonPackage rec {
    pname = "pyobjc-core";
    version = "7.3";
    adfasdf = true;
    name = "${pname}-${version}";
    src = py-pkgs.fetchPypi {
      pname = "pyobjc-core";
      inherit version;
      sha256 = "0x3msrzvcszlmladdpl64s48l52fwk4xlnnri8daq2mliggsx0ah";
    };
    # setupPyBuildFlags = [ "-j" "12" ] ;
    #makeWrapperArgs = ["--set SDKROOT /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"];
    preBuild=''
         export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk"
    '';
    CFLAGS = "-iwithsysroot /usr/include -Wno-unused-argument";
    doCheck = false;
    commonPreHook = ''
      export SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    '';
    propagatedBuildInputs = [
      pkgs.darwin.libobjc
      pkgs.darwin.cctools
      pkgs.darwin.apple_sdk.frameworks.Foundation
      pkgs.darwin.apple_sdk.frameworks.AppKit
      pkgs.darwin.apple_sdk.frameworks.CoreServices
      pkgs.darwin.apple_sdk.frameworks.Cocoa
      py-pkgs.setuptools
    ];
    buildInputs = [ pkgs.libffi ];
  };

  cocoa = py-pkgs.buildPythonPackage rec {
    pname = "pyobjc";
    version = "7.3";
    name = "${pname}-${version}";
    src = py-pkgs.fetchPypi {
      pname = "pyobjc-framework-Cocoa";
      inherit version;
      sha256 = "0zhbp18i06aprwfbp06l9wm3qrzsdcyy9hwis5d4b8wmlzkhb3di"; # 7.3 Cocoa
    };
    CFLAGS="-Wno-unused-argument -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX10.12.sdk";
    doCheck = false;
    setupPyBuildFlags = [ "-j" "12" ] ;
    propagatedBuildInputs = [
      pyobjc-core
      pkgs.darwin.libobjc
      pkgs.darwin.apple_sdk.frameworks.Foundation
      pkgs.darwin.apple_sdk.frameworks.CoreFoundation
      pkgs.darwin.apple_sdk.frameworks.AppKit
      pkgs.darwin.apple_sdk.frameworks.Cocoa
      pkgs.darwin.apple_sdk.frameworks.CoreServices
      py-pkgs.setuptools
    ];
  };

  my-send2trash = py-pkgs.buildPythonPackage rec {
    name = "send2trash";
    version = "1.7.1";

    doCheck = !pkgs.stdenv.isDarwin;
    setupPyBuildFlags = [ "-j" "12" ] ;
    src = pkgs.fetchFromGitHub {
      owner = "arsenetar";
	    repo = "${name}";
	    rev = "${version}";
	    sha256 = "04fyax0jbqvichv1w5gbi5jy3zj3p2npdqq8k2prripl7xam6na9";
    };

    buildInputs = (with py-pkgs; [ pyobjc-core cocoa setuptools pytest ]);
  };


  pytest-ovr = hf: hs: {
    pytest = hs.pytest.overridePythonAttrs(old: {
      # python = myPatchedPython;
      doCheck = false;
    });
    toml = hs.toml.overridePythonAttrs(old: {
      # python = myPatchedPython;
      doCheck = false;
    });
    semver = hs.pytest.overridePythonAttrs(old: {
      # python = myPatchedPython;
      doCheck = false;
    });
  };

  py39 = (py-pkgs).override (
    old: {
      overrides = builtins.foldl' pkgs.lib.composeExtensions (old.overrides or (_: _: {}))
        [ pytest-ovr ];
    }
  );

in (myPatchedPython.withPackages (p: with p; [ pygogo my-send2trash ])).env
